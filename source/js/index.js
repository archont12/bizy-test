import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import * as asyncInitialState from 'redux-async-initial-state';
import 'babel-polyfill';
import logger from 'dev/logger';

import rootReducer from 'reducers';
import Routes from 'routes';

const isProduction = process.env.NODE_ENV === 'production';

// Creating store
let store = null;
let middleware = null;

const loadStore = () => {
  return new Promise(resolve => {
    fetch('http://localhost:8000/birds')
      .then(response => response.json())
      .then(birds => {
        resolve({
          birds,
        })
      });
  });
}

if (isProduction) {
  // In production adding only thunk middleware
  middleware = applyMiddleware(
    asyncInitialState.middleware(loadStore),
    thunk
  );

} else {
  // In development mode beside thunk
  // logger and DevTools are added
  middleware = applyMiddleware(
    asyncInitialState.middleware(loadStore),
    thunk,
    logger
  );

  let enhancer;

  // Enable DevTools if browser extension is installed
  if (window.__REDUX_DEVTOOLS_EXTENSION__) { // eslint-disable-line
    enhancer = compose(
      middleware,
      window.__REDUX_DEVTOOLS_EXTENSION__() // eslint-disable-line
    );
  } else {
    enhancer = compose(middleware);
  }
}

// Create Store
store = createStore(
  rootReducer,
  middleware
);

// Render it to DOM
ReactDOM.render(
  <Provider store={store}>
    <Routes />
  </Provider>,
  document.getElementById('root')
);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import FuzzySearch from 'fuse.js';
import Autocomplete from '../../components/Autocomplete';

@connect(state => ({
  birds: state.app.get('birds'),
}))
export default class Dashboard extends Component {
  static propTypes = {
    birds: PropTypes.array,
    dispatch: PropTypes.func,
  }

  constructor(props) {
    super(props);

    this.getBirdsArr = this.getBirdsArr.bind(this);
    this.searchBirds = this.searchBirds.bind(this);
    this.fuzzySearch = null;
    this.birds = null;
    this.searchOptions = {
      shouldSort: true,
      threshold: 0.5,
      location: 0,
      distance: 200,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: []
    };
  }

  /*
   * Convert birds List to Array
   * @returns Array
   */
  getBirdsArr() {
    const { birds } = this.props;
    return List(birds).toArray();
  }

  /*
   * Search for birds using fuzzy search
   *
   * @param String - query
   * @param Number - limit
   * @returns Array
   */
  searchBirds(query, limit) {
    // Get birds
    this.birds = this.birds || this.getBirdsArr();

    // Create fuzzy search module instance
    this.fuzzySearch = this.fuzzySearch || new FuzzySearch(
      this.birds,
      this.searchOptions
    );
    const result = this.fuzzySearch.search(query);
    const birdsResult = [];

    if(result.length < limit) {
      limit = result.length;
    }

    for(let i = 0; i < limit; i++) {
      birdsResult.push(this.birds[result[i]]);
    }

    return birdsResult;
  }

  render() {
    const autocompleteData = {
      search: this.searchBirds,
      limit: 5,
    };

    return (
      <div>
        <Autocomplete {...autocompleteData} />
      </div>
    );
  }
}

export default {
  base: {
    margin: '20px auto',
    maxWidth: '500px',
    width: 'calc(100% - 40px)',
    padding: '20px',
    borderRadius: '3px'
  }
};

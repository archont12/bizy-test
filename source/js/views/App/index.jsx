import React, { Component } from 'react';
import Radium from 'radium';
import PropTypes from 'prop-types';
import styles from './styles.js';

@Radium
export default class App extends Component {
  static propTypes = {
    children: PropTypes.object,
  }

  render() {
    const { children } = this.props;

    return (
      <div style={[styles.base]}>
        <div>
          { children }
        </div>
      </div>
    );
  }
}

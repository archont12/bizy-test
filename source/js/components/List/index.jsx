import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Radium from 'radium';
import styles from './styles.js';

@Radium
export default class List extends Component {

  static propTypes = {
    list: PropTypes.array,
    onClick: PropTypes.func,
    visible: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    this.getList = this.getList.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  getList() {
    const result = [];
    const { list } = this.props;

    for(let i = 0; i < list.length; i++) {
      result.push(
        <li key={i} style={styles.li}>
          {list[i]}
        </li>
      );
    }

    return result;
  }

  handleClick(event) {
    const element = event.target.innerHTML;

    this.props.onClick(element);
  }

  render() {
    if(!this.props.visible) {
      return null;
    }

    const list = this.getList();

    return (
      <div>
        <ul style={styles.ul} onClick={this.handleClick}>{list}</ul>
      </div>
    );
  }
}

export default {
  ul: {
    minWidth: '494px',
    top: '44px',
    border: '2px solid rgba(0,0,0,0.08)',
    textAlign: 'left',
    left: '0px',
    marginTop: '-3px',
    padding: '11px',
    listStyleType: 'none',
  },
  li: {
    padding: '3px',
    ':hover': {
      background: 'rgba(0,0,0,0.08)',
      cursor: 'pointer',
    },
  },
};

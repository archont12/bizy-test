import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Radium from 'radium';
import List from '../List';
import styles from './styles.js';

@Radium
export default class Autocomplete extends Component {

  static propTypes = {
    search: PropTypes.func,
    limit: PropTypes.number,
  };

  constructor(props) {
    super(props);

    this.state = {
      values: [],
      chosenValue: '',
    };

    this.handleType = this.handleType.bind(this);
    this.handlePickElement = this.handlePickElement.bind(this);
  }

  handleType(event) {
    const query = event.target.value;
    const { search, limit } = this.props;

    this.setState({
      values: search(query, limit),
      chosenValue: query,
    });
  }

  handlePickElement(element) {
    this.setState({
      chosenValue: element,
      values: [],
    });
  }

  render() {
    const listProps = {
      list: this.state.values,
      onClick: this.handlePickElement,
      visible: this.state.values.length === 0 ? false : true,
    };

    return (
      <div style={styles.wrapper}>
        <input type={'text'} style={styles.input} value={this.state.chosenValue} onChange={this.handleType} />
        <div>
          <List {...listProps} />
        </div>
      </div>
    );
  }
}

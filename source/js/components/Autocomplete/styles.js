export default {
  input: {
    border: '1px solid rgba(0,0,0,0.08)',
    borderRadius: '2px',
    boxShadow: '0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08)',
    padding: '4px',
    margin: '0px',
    width: '100%',
    left: '0px',
    outline: 'none',
    lineHeight: '34px',
    height: '34px !important',
    paddingLeft: '14px',
  },
  wrapper: {
    marginTop: '25%',
  },
};

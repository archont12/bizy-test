import { Map } from 'immutable';

import {
  STATE_LOADING_DONE,
} from 'actions/app';

const initialState = Map({
  birds: []
});

const actionsMap = {
  [STATE_LOADING_DONE]: (state, action) => {
    return state.merge({
      birds: action.payload.state.birds
    });
  }
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}

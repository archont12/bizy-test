# Bizy Test Task

## Features

This is a micro project with react + redux architecture. This project include Autocomplete component which is using a fuzzy search, so a query string support searching with typos. 
Fuzzy search based on fuse.js library, which is using a bitap algorithm. The algorithm tells whether a given text contains a substring which is "approximately equal" to a given pattern, where approximate equality is defined in terms of Levenshtein distance.

## Setup

[Install](https://yarnpkg.com/en/docs/install) yarn package manager

```
$ yarn
```

## Running in dev mode

```
$ npm start
```

Running project on `http://localhost:3000/`.

Running server with mock data on `http://localhost:8000/birds`.


## Linting

For linting I'm using [eslint-config-airbnb](https://www.npmjs.com/package/eslint-config-airbnb),

```
$ npm run lint
```

## Unit Testing


```
npm run test
```